import React from 'react';
import {useNavigate} from "react-router-dom";

const UserItem = (props) => {
    const navigate = useNavigate()
    return (
        <div className="card">
            <img src={props.user.avatar} alt='Аватар'/>
            <div className="card-body">
                <ul className="card-body">
                    <li>ID: {props.user.id}</li>
                    <li>ФИО: {props.user.first_name} {props.user.last_name}</li>
                    <li>@: {props.user.email}</li>
                </ul>
                <button className="profile-btn" onClick={() => navigate(`/users/${props.user.id}`)}>Перейти в профиль</button>
            </div>
        </div>
    );
};

export default UserItem;