import React from 'react';

const UserProfile = (props) => {
    return (
        <div className="card" style={{alignItems: 'center', flexDirection: 'column'}}>
            <div className="card-title">Вы зашли в профиль пользователя {props.user.first_name}:{props.user.id}</div>
            <img src={props.user.avatar} alt='Аватар'/>
            <div className="card-body">
            <ul>
                <li>ID: {props.user.id}</li>
                <li>ФИО: {props.user.first_name} {props.user.last_name}</li>
                <li>@: {props.user.email}</li>
            </ul>
            </div>
        </div>
    );
};

export default UserProfile;