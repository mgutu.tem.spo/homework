import {Fragment, useEffect, useState} from "react";
import UserItem from "./UserItem";
import UserService from "../api/UserService";
import {useFetching} from "../hooks/useFetching";

const UserList = () => {

    const [users, setUsers] = useState([])

    const [fetchUsers] = useFetching(async () => {
        const users = await UserService.getAll()
        setUsers(users)
    })

    useEffect(() => {
        fetchUsers()
    })

    return (
        <Fragment>
            <h1 className='user-list'>Список пользователей</h1>
            <hr/>
            <div className="content">
                {users.map(user => <UserItem user={user} key={user.id}/> )}
            </div>
        </Fragment>
    )
}

export default UserList;