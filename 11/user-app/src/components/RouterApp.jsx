import React from 'react';
import {Navigate, Route, Routes} from "react-router-dom";
import {routes} from "../router";

const RouterApp = () => {
    return (
        <Routes>
            {routes.map(route =>
                <Route
                    path={route.path}
                    element={route.element}
                    exact={route.exact}
                />
            )};
            <Route path="*" element={<Navigate to="/users"/>}/>
        </Routes>
    );
};

export default RouterApp;