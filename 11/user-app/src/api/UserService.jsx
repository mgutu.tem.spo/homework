import axios from "axios";

export default class UserService {
    static async getAll() {
        const response = await axios.get('https://reqres.in/api/users', )
        return response.data.data
    }
    static async getById(id) {
        const response = await axios.get('https://reqres.in/api/users/' + id)
        return response.data.data
    }
}