import React, {Fragment, useEffect, useState} from 'react';
import UserProfile from "../components/UserProfile";
import {useParams} from "react-router-dom";
import UserService from "../api/UserService";
import {useFetching} from "../hooks/useFetching";

const DetailUser = () => {
    const params = useParams()
    const [user, setUsers] = useState([])

    const [fetchPostById] = useFetching(async (id) => {
        const response = await UserService.getById(id)
        setUsers(response)
    })

    useEffect(() => {
        fetchPostById(params.id)
    })

    return (
        <Fragment>
            <UserProfile user={user} key={params.id}/>
        </Fragment>
    );
};

export default DetailUser;