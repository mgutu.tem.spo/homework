import React, {Fragment} from 'react';
import UserList from "../components/UserList";

const ListUsers = () => {
    return (
        <Fragment>
            <UserList/>
        </Fragment>
    );
};

export default ListUsers;