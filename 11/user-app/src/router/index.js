import ListUsers from "../pages/ListUsers";
import DetailUser from "../pages/DetailUser";


export const routes = [
    {path: '/users', element: <ListUsers/>, exact: true},
    {path: '/users/:id', element: <DetailUser/>, exact: true}
]
