import './css/style.css';
import {BrowserRouter} from "react-router-dom";
import RouterApp from "./components/RouterApp";

function App() {

    return (
        <BrowserRouter>
            <RouterApp/>
        </BrowserRouter>
    );
}

export default App;
