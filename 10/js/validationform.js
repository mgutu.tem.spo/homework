const form = document.querySelector('.register-form'),
    formInputs = document.querySelectorAll('.input-form'),
    inputEmail = document.querySelector('.input-email'),
    inputPassword = document.querySelector('.input-password'),
    inputCheckbox = document.querySelector('.input-checkbox');


function validateEmail(email) {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

form.addEventListener('submit', function (e) {
    e.preventDefault()

    const errors = form.querySelectorAll('.error-info')
    
    errors.forEach(function (error) {
        error.remove()
    })

    formInputs.forEach(function (input) {
        if (input.value === '') {
            console.log('Поля пустые')
            input.classList.add('input-error');
            const errorBox = document.createElement('div')
            errorBox.className = 'error-info'
            errorBox.innerHTML = 'Поле обязательно для заполнения'
            input.parentElement.insertAdjacentElement('afterend', errorBox)
        }
        else {
            input.classList.remove('input-error')
        }
    })

    if (inputPassword.value.length < 8) {
        inputPassword.classList.add('input-error');
        console.log('Пароль меньше 8 символов')
        const errorBox = document.createElement('div')
        errorBox.className = 'error-info'
        errorBox.innerHTML = 'Пароль должен содержать как минимум 8 символов'
        inputPassword.parentElement.insertAdjacentElement('afterend', errorBox)
        return false;
    }

    if (!validateEmail(inputEmail.value)) {
        console.log('Email не валидный')
        const errorBox = document.createElement('div')
        errorBox.className = 'error-info'
        errorBox.innerHTML = 'Введите корректный email'
        inputEmail.parentElement.insertAdjacentElement('afterend', errorBox)
        return false;
    }
    else {
        inputEmail.classList.remove('input-error')
    }

    if (!inputCheckbox.checked) {
        inputCheckbox.classList.add('input-error')
        const errorBox = document.createElement('div')
        errorBox.className = 'error-info'
        errorBox.innerHTML = 'Поле обязательно для заполнения'
        inputCheckbox.parentElement.insertAdjacentElement('afterend', errorBox)
        return false;
    } else {
        inputCheckbox.classList.remove('input-error')
    }

    const data = {
        password: inputPassword.value,
        email: inputEmail.value,
        privacy: inputCheckbox.checked
    };

    console.log(data)

})
