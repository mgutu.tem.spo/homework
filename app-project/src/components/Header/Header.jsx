import React, {useContext} from 'react';
import {Link} from "react-router-dom";
import {AuthContext} from "../../context";

const Header = () => {
    const {isAuth, setIsAuth} = useContext(AuthContext)

    const logout = () => {
        setIsAuth(false);
        localStorage.removeItem('auth');
    }

    return (
        <header>
            <div className="brand">
                <img src="images/logo.png" alt="Logo"/>
                <div className="brand-block">
                    <Link to="/"><h1 className="brand-text">OnlineProjSchool</h1></Link>
                    <small>Онлайн школа программирования им.Денисенко С.Е.</small>
                </div>
            </div>
            <nav className="menu-nav">
                <ul>
                    <li><Link to="/">Главная</Link></li>
                    <li><Link to="/about">О нас</Link></li>
                    <li><Link to="/users">Пользователи</Link></li>
                    {isAuth
                    ? <li onClick={logout}>Выйти</li>
                    : <li><Link to="/login">Войти на сайт</Link></li>}
                </ul>
            </nav>
        </header>
    );
};

export default Header;