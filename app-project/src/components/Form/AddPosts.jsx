import React, {useState} from 'react';
import MyButton from "../UI/button/MyButton";
import MyInput from "../UI/input/MyInput";

const PostForm = ({create}) => {
    let [post, setPost] = useState({title: '', description: ''})

    const addNewPost = (e) => {
        e.preventDefault()
        let newPost = {...post, id: Date.now()}
        create(newPost)
        setPost({title: '', description: '', author: '', price: '', image: ''})
    }

    return (
        <div className="ops-form">
            <form>
                <div className="col">
                    <label htmlFor="title">Название</label>
                    <MyInput id="title" type="text" value={post.title}
                           onChange={event => setPost({...post, title: event.target.value})} placeholder="Название"/>
                </div>
                <div className="col">
                    <label htmlFor="price">Цена</label>
                    <MyInput id="price" type="text" value={post.price}
                           onChange={event => setPost({...post, price: event.target.value})} placeholder="Цена"/>
                </div>
                <div className="col">
                    <label htmlFor="description">Описание</label>
                    <MyInput type="text" placeholder="Описание урока" value={post.description}  onChange={event => setPost({...post, description: event.target.value})}/>
                </div>
                <div className="col">
                    <label htmlFor="tags">Теги</label>
                    <MyInput type="text" placeholder="Теги" value={post.tag}  onChange={event => setPost({...post, tag: event.target.value})}/>
                </div>
                <div className="col">
                    <label htmlFor="time">Время</label>
                    <MyInput type="text" placeholder="Время на изучение" value={post.time}  onChange={event => setPost({...post, time: event.target.value})}/>
                </div>
                <div className="col">
                    <label htmlFor="author">Автор</label>
                    <MyInput type="text" placeholder="Автор" value={post.author}  onChange={event => setPost({...post, author: event.target.value})}/>
                </div>
                <MyButton onClick={addNewPost}>Создать пост</MyButton>
            </form>
        </div>
    );
};

export default PostForm;