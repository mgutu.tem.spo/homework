import React from 'react';
import CardItem from "./CardItem";

const CardList = ({posts, remove}) => {
    if(!posts.length) {
        return (
            <div>
                Посты не найдены...
            </div>
        )
    }
    return (
        <div className="row">
            {posts.map(post => <CardItem remove={remove} post={post} key={post.id}/>)}
        </div>
    );
};

export default CardList;