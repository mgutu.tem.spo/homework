import React from 'react';
import MyButton from "../../UI/button/MyButton";

const CardItem = (props) => {
    return (
        <div className="card">
            <h2 className="card-title">{props.post.title}</h2>
            <div className="card-img-block">
                <img src={props.post.image} alt={props.post.title}/>
                <div className="card-price">{props.post.price}</div>
                <MyButton style={{marginLeft: '5px'}} onClick={() => props.remove(props.post)}>Удалить пост</MyButton>
            </div>
            <div className="card-description">
                {props.post.description}
            </div>
            <div className="card-footer">
                <span className="author">🧐 {props.post.author}</span>
                <span className="time">⌚ {props.post.time}</span>
                <span className="tags">#️⃣ {props.post.tag}</span>
            </div>
        </div>
    );
};

export default CardItem;