import React from 'react';
import {useNavigate} from "react-router-dom";
import MyButton from "../UI/button/MyButton";

const CardUsersItem = (props) => {
    const navigate = useNavigate()
    return (
        <div className="card">
            <h2 className="card-title">{props.user.first_name} {props.user.last_name}</h2>
            <div className="card-img-block users">
                <img src={props.user.avatar} alt={props.user.name}/>
            </div>
            <div className="card-description">
                {props.user.email}
            </div>
            <div className="card-footer">
                <MyButton onClick={() => navigate(`/users/${props.user.id}`)}>Перейти в профиль</MyButton>
            </div>
        </div>
    );
};

export default CardUsersItem;