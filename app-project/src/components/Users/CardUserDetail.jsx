import React from 'react';
import {Link} from "react-router-dom";

const CardUserDetail = (props) => {
    return (
        <div>
            <div className="card">
                <h2 className="card-title">{props.user.first_name} {props.user.last_name}</h2>
                <div className="card-img-block users">
                    <img src={props.user.avatar} alt={props.user.name}/>
                </div>
                <div className="card-description">
                    {props.user.email}
                </div>
                <div className="card-footer">
                    <ul>
                        <li>
                            <Link to={'/posts'}>К постам</Link>
                        </li>
                        <li>
                            <Link to={'/users'}>К пользователям</Link>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    );
};

export default CardUserDetail;