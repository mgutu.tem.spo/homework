import React from 'react';
import CardUsersItem from "./CardUsersItem";

const CardUsersList = ({users}) => {
    return (
        <div className="row">
            {users.map(user => <CardUsersItem user={user} key={user.id}/>)}
        </div>
    );
};

export default CardUsersList;