import React, {Fragment, useEffect, useState} from 'react';
import {useFetching} from "../hooks/useFetch";
import UserService from "../api/userService";
import CardUsersList from "../components/Users/CardUsersList";

const Users = () => {
    const [users, setUsers] = useState([])

    // Кастомный хук
    const [fetchUsers] = useFetching(async () => {
        const users = await UserService.getAll()
        setUsers(users)
    })

    // Загрузка при входе на страницу, единожды
    useEffect(() => {
        fetchUsers()
    }, []) // eslint-disable-line react-hooks/exhaustive-deps

    return (
        <Fragment>
            <CardUsersList users={users}/>
        </Fragment>
    );
};

export default Users;