import React from 'react';

const About = () => {
    return (
        <div className="about">
            Добро пожаловать на наш сайт онлайн-школы программирования! С Вами, С.Е. Денисенко, и мы демонстрируем наш первый проект!
        </div>
    );
};

export default About;