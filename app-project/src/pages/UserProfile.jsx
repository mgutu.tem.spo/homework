import React, {Fragment, useEffect, useState} from 'react';
import {useParams} from "react-router-dom";
import {useFetching} from "../hooks/useFetch";
import UserService from "../api/userService";
import CardUserDetail from "../components/Users/CardUserDetail";

const UserProfile = () => {
    const params = useParams()
    const [user, setUsers] = useState([])

    const [fetchPostById] = useFetching(async (id) => {
        const response = await UserService.getById(id)
        setUsers(response)
    })

    useEffect(() => {
        fetchPostById(params.id)
    }, []) // eslint-disable-line react-hooks/exhaustive-deps

    return (
        <Fragment>
            <CardUserDetail user={user} key={user.id}/>
        </Fragment>
    );
};

export default UserProfile;