import React, {useContext} from 'react';
import MyButton from "../components/UI/button/MyButton";
import {AuthContext} from "../context";
import MyInput from "../components/UI/input/MyInput";

const Login = () => {
    const {setIsAuth} = useContext(AuthContext)

    const login = event => {
        event.preventDefault();
        setIsAuth(true);
        localStorage.setItem('auth', 'true');
    }

    return (
        <div className="ops-form">
            <div className="alert">
                Чтобы просматривать контент, необходимо авторизоваться на нашем сайте. ✔️
            </div>
            <form>
                <h2>Авторизация на сайте</h2>
                <div className="row column">
                    <div className="col">
                        <label htmlFor="login">Логин (demo)</label>
                        <MyInput type="text" id="login" value="demo"/>
                    </div>
                    <div className="col">
                        <label htmlFor="password">Пароль (demo)</label>
                        <MyInput type="password" id="password" value="demo"/>
                    </div>
                </div>
                <MyButton onClick={login}>✨ Войти</MyButton>
            </form>
        </div>
    );
};

export default Login;