import CardList from "../components/Posts/Card/CardList";
import {Fragment, useState} from "react";
import MyButton from "../components/UI/button/MyButton";
import MyModal from "../components/UI/modal/MyModal";
import PostForm from "../components/Form/AddPosts";
import PostFilter from "../components/Posts/PostFilter";
import {usePosts} from "../hooks/usePosts";

const Home = () => {
    // Массив постов
    const [posts, setPosts] = useState([
        {id: 1, title: 'Учим чистый JavaScript', image: 'images/posts/1.png', author: 'dse405', price: 'Free', description: 'В этой теме мы разберем методы работы с JavaScript', time: '1 час', tag: 'дизайн, динамика'},
        {id: 2, title: 'HTML/CSS за час', image: 'images/posts/html.webp', author: 'dse405', price: 'Free', description: 'Изучим типовое построение HTML страницы с минимальными стилями', time: '1 час', tag: 'шаблоны, статика'},
        {id: 3, title: 'TypeScript за час', image: 'images/posts/typescript.png', author: 'dse405', price: 'Free', description: 'Изучим строгую типизацию TypeScript', time: '1 час', tag: 'типизация, js'},
        {id: 4, title: 'Работаем на C# в Unity', image: 'images/posts/c_sh.webp', author: 'dse405', price: '5000р', description: 'Научимся работать с моделями и скриптами в Unity', time: '3 часа', tag: 'игры, unity'},
        {id: 5, title: 'Selenium на Python для сбора данных с динамическими формами', image: 'images/posts/python.webp', author: 'dse405', price: '4000р', description: 'В этом уроке разберем, как правильно собирать информацию со страниц с формой входа', time: '40 минут', tag: 'scrapy, web'},
        {id: 6, title: 'Создаем блокнот на C++', image: 'images/posts/cplus.webp', author: 'dse405', price: 'Free', description: 'Создадим минималистичный блокнот на языке C++', time: '15 минут', tag: 'programming'},
        {id: 7, title: 'HTML/CSS за час', image: 'images/posts/html.webp', author: 'Dmitry', price: 'Free', description: 'Тест', time: '1 час', tag: 'html, css'},
        {id: 8, title: 'HTML/CSS за час', image: 'images/posts/html.webp', author: 'dse405', price: 'Free', description: 'Тест', time: '1 час', tag: 'html, css'},
    ])
    // Модальное окно
    const [modal, setModal] = useState(false);
    // Состояние сортировки
    const [filter, setFilter] = useState({sort: '', query: ''})

    // Добавление постов
    const createPost = (newPost) => {
        setPosts([...posts, newPost]);
        setModal(false);
    }

    // Удаление постов
    const removePost = (post) => {
        setPosts(posts.filter(p => p.id !== post.id))
    }

    // Кастомный хук с логикой сортировки и поиска
    const sortedAndSearchPosts = usePosts(posts, filter.sort, filter.query)

    return (
        <Fragment>
            <div className="sidebar">
                <MyButton style={{marginBottom: '20px'}} onClick={() => setModal(true)}>
                    Добавить материал
                </MyButton>
                <MyModal visible={modal} setVisible={setModal}>
                    <PostForm create={createPost}/>
                </MyModal>
                <PostFilter filter={filter} setFilter={setFilter}/>
            </div>
            <CardList remove={removePost} posts={sortedAndSearchPosts}/>
        </Fragment>
    );
};

export default Home;