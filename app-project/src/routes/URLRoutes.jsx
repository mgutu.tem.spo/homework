import About from "../pages/About";
import Login from "../pages/Login";
import Home from "../pages/Home";
import Users from "../pages/Users";
import UserProfile from "../pages/UserProfile";


export const privateRoutes = [
    {path: '/', element: <Home/>, exact: true},
    {path: '/about', element: <About/>, exact: true},
    {path: '/posts', element: <Home/>, exact: true},
    {path: '/users', element: <Users/>, exact: true},
    {path: '/users/:id', element: <UserProfile/>, exact: true}
]

export const publicRoutes = [
    {path: '/about', element: <About/>, exact: true},
    {path: '/login', element: <Login/>, exact: true},
    {path: '/users', element: <Users/>, exact: true},
    {path: '/users/:id', element: <UserProfile/>, exact: true}
]