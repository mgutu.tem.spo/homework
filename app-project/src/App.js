import {BrowserRouter} from "react-router-dom";
import AppRouter from "./routes/AppRouter";
import Header from "./components/Header/Header";
import {useEffect, useState} from "react";
import {AuthContext} from "./context";

function App() {
    const [isAuth, setIsAuth] = useState(false);

    useEffect(() => {
        if(localStorage.getItem('auth')) {
            setIsAuth(true)
        }}, [])

      return (
          <AuthContext.Provider value={{
              isAuth,
              setIsAuth
          }}>
          <BrowserRouter>
              <Header/>
              <div className="content">
                <AppRouter/>
              </div>
          </BrowserRouter>
          </AuthContext.Provider>
      );
}

export default App;
